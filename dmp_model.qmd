---
title: Data Mangement Plan (DMP)
subtitle: "structure: TBI"
format:
  pdf:
    toc: true
    toc-depth: 2
    number-sections: true
    keep-tex: true
    
editor: visual
---

{{< pagebreak >}}

![](img/Logo-TBI-inf3.jpg){fig-align="center"}

::: {#tut_logos layout="[[20, 70, 20]]" layout-valign="center"}
![](img/Logo-INRAE.png){width="170"}

![](img/Logo_INSAvilletoulouse-crop.jpg){width="101"}

![](img/LOGO_CNRS_2019_RVB.png){width="75"}
:::

::: {layout="[[10, 30, 10]]"}
 

Licence for this document: ![](img/cc_by_88x31.png){fig-align="center" width="71"}

 
:::

## Information about Structure

### Identification

TBI: [Toulouse Biotechnology Institute](https://www.toulouse-biotechnology-institute.fr/)

UMR 712 TBI INSA/INRA

UMR 5504 INSA/CNRS

### Structure type

Scientific laboratory under mixed tutorship: INSA, INRAE and CNRS

### Responsabilities

Director

:   Gilles TRUAN

Deputy Directors

:   Mathieu SPERANDIO, Emmanuelle TREVISIOL

RDO (Référent Données Opérationnelles)

:   Serguei Sokol

### TBI presentation

TBI is organized in four scientific poles and technological one:

Biocatalysis

:   -   study the structure, activity and dynamics of enzymes;
    -   guide protein design.

Physiology and engineering of microbial metabolism pole

:   -   characterization of the molecular basis of microbial metabolisms, natural and synthetic;
    -   its applications in sustainable biotechnology.

Bioprocess and Microbial Engineering pole

:   \
    Understand the microorganism(s)-process-environment interactions that limit bioprocess performance, robustness and sustainability.

Sustainable Process Engineering pole

:   -   Better knowledge of the limiting phenomena that determine the performance of the process

    -   Multi-scale modeling (scale-up: local process)

    -   Preuves de concept pour la faisabilité du procédé dans différents domaines d'application (Industrie chimique, biotechnologies, traitement de l'eau, etc.)

    -   Eco-designed technical solutions

Technological Pole

:   \
    State of the art facilities

## Information about this DMP

DOI (version published on dataINRAE):

:   <https://…>

Version:

:   TBI_V0_EN

Edited by:

:   Data referents from research teams/platforms (RDEP) and Serguei SOKOL (RDO)

Content organization:

:   Data are described as relative to *research objects* and following *six steps* of data life cycle. Each team/platform is indicated as user of particular research object and its data but also as using particular tool/method during data life cycle.

    This document is produced in automatic way from input xlsx file having one research object description per spreadsheet. If any step is empty in this file, it is not included in this final document.

    This DMP organization and production are chosen for high degree of flexibility and factorization of common research object and corresponding tools & methods.

```{r}
#| echo: false
#| results: asis

source("xlsx2dmp.R")
main("processus bacterial genomes.xlsx")
```

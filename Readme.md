# Converting XLSX to Data Management Plan (DMP)

This project aims to facilitate DMP creation by automation of the process of collecting and formatting information about data. User can describe the life cycle of his data (research object in terms of DMP) in an XLSX file (cf. for example `processus bacterial genomes.xlsx`) then write a general presentation of his lab or project in a QMD (Quarto MarkDown) format (cf. `dmp_model.qmd`). This latter can be done e.g. in Rstudio, (a free software running R and other programming languages). Once done, clicking on "Render" button produces a PDF file.

User is not supposed to have programming skills. He has to edit two documents:: :

 - XLSX file with research objects (one object per sheet);
 - QMD file with introduction to his DMP;
 
In this last file, in the end, he has to replace the name of example XLSX file by the name of his own XLSX file.

Author: Sergueï SOKOL, TBI/MICA/INRAE

Copyright: INRAE/INSA/CNRS

Licenses: GPL2 for R code, CC-BY for documents

Credits: PPTX file `processus bacterial genomes.pptx` (on which this work is based on) was created and shared by Sophie Payot-Lacroix, unit "Dynamic", INRAE.
